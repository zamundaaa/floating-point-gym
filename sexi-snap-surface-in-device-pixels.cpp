#include <cmath>
#include <iostream>

static int snap(int value, int unit)
{
    return (value / unit) * unit;
}

static int map(int value, int atom)
{
    return value / atom;
}

int main()
{
    for (double scale = 1.0; scale < 3.0; scale += 0.05) {
        for (int width = 5 * 60; width < 15 * 60; width += 1) {
            const int atom = 60 / scale;
            const int unit = std::max(1, 60 / atom) * atom;

            const int left_border = unit;
            const int right_border = unit;
            const int client = snap(width, unit) - left_border - right_border;

            const int left_border_left_native = map(0, atom);
            const int left_border_right_native = map(left_border, atom);

            const int client_left_native = left_border_right_native + map(0, atom);
            const int client_right_native = left_border_right_native + map(client, atom);

            const int right_border_left_native = map(left_border + client, atom);
            const int right_border_right_native = map(left_border + client + right_border, atom);

            const bool gap = client_right_native != right_border_left_native;
            const bool collapsed = right_border_left_native == right_border_right_native;

            if (gap) {
                std::cout << "gap (@" << scale << ") for " << width << std::endl;
            } else if (collapsed) {
                std::cout << "collapsed border (@" << scale << ") for " << width << std::endl;
            }
        }
    }

    return 0;
}
