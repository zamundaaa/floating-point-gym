#include <iostream>

int main()
{
    // float(a) + float(b) != float(a + b)

    const double a = 0.1 + (0.2 + 0.3); // float(float(0.1) + float(float(0.2) + float(0.3)))
    const double b = (0.1 + 0.2) + 0.3; // float(float(float(0.1) + float(0.2)) + float(0.3))
    std::cout << (a == b) << std::endl;
    return 0;
}
