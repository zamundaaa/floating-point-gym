#include <cmath>
#include <iostream>

struct Segment
{
    int offset;
    int extent;
};

/**
 * Issue: preserves the size but introduces gaps between consecutive segments
 */
static Segment snapToPixelGrid1(const Segment &segment, double scale)
{
    return Segment {
        .offset = std::round(segment.offset * scale),
        .extent = std::round(segment.extent * scale),
    };
}

/**
 * Issue: no gaps between consecutive segments, but shrinks or stretches the segments
 */
static Segment snapToPixelGrid2(const Segment &segment, double scale)
{
    return Segment {
        .offset = std::round(segment.offset * scale),
        .extent = std::round((segment.offset + segment.extent) * scale) - std::round(segment.offset * scale),
    };
}

/**
 * Issue: introduces gaps
 */
static Segment snapToPixelGrid3(const Segment &segment, double scale)
{
    return Segment {
        .offset = std::ceil(segment.offset * scale),
        .extent = std::ceil(segment.extent * scale),
    };
}

std::ostream &operator<<(std::ostream &stream, const Segment &segment)
{
    stream << "Segment{";
    stream << "offset: " << segment.offset;
    stream << ", ";
    stream << "extent: " << segment.extent;
    stream << "}";
    return stream;
}

int main()
{
    const double scale = 1.75;

    for (int offset = 0; offset < 5; ++offset) {
        for (int extent = 1; extent < 5; ++extent) {
            const Segment surface {
                .offset = offset,
                .extent = extent,
            };
            const Segment snappedSurface = snapToPixelGrid2(surface, scale);

            const Segment border {
                .offset = offset + extent,
                .extent = 1,
            };
            const Segment snappedBorder = snapToPixelGrid2(border, scale);

            const bool gap = snappedSurface.offset + snappedSurface.extent != snappedBorder.offset;
            const bool surfaceDistorted = snappedSurface.extent != std::round(surface.extent * scale);
            const bool borderDistorted = snappedBorder.extent != std::round(border.extent * scale);
            if (gap || surfaceDistorted || borderDistorted) {
                std::cout << "***";
                if (gap) {
                    std::cout << " {gap}";
                }
                if (surfaceDistorted) {
                    std::cout << " {distorted surface}";
                }
                if (borderDistorted) {
                    std::cout << " {distorted border}";
                }
                std::cout << std::endl;

                std::cout << "surface: " << surface << std::endl;
                std::cout << "border:  " << border << std::endl;
                std::cout << "  snapped surface: " << snappedSurface << std::endl;
                std::cout << "  snapped border:  " << snappedBorder << std::endl;
            }
        }
    }

    return 0;
}
