#include <cmath>
#include <iostream>

int main()
{
    const int multiplier = 60;
    for (double scale = 0.5; scale < 3; scale += 0.05) {
        std::cout << scale << " -> " << std::round(scale * multiplier) << "(" << double(scale * multiplier) << ")" << std::endl;
    }
    return 0;
}
