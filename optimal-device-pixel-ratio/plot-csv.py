import matplotlib.pyplot as plt
import pandas as pd


dataset = pd.read_csv("stats.csv", usecols=["base", "mismatches"])
plt.plot(dataset.base, dataset.mismatches)
plt.show()
