# Idea

- represent logical geometry values as multiples of 1/60
- only allow geometry updates that are multiple of 1 physical pixel
- with some scale factors (e.g. 1.75), it's impossible => nudge the scale factor until it's possible (~1.76)

![plot](Figure_1.png)

The logical coordinate system scale factor is chosen empirically. The graph above shows the number of scale factors (in the 1-2 interval, with the 0.05 delta) that can't be represented precisely with the given coordinate system scale factor.
