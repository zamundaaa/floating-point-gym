#include <cmath>
#include <iostream>

int main()
{
    const int leftBorder = 1;
    const int width = 5;
    const int rightBorder = 1;

    const double scale = 1.75;

    for (int x = 0; x < 50; ++x) {
        const int nativeLeftBorder = std::round(leftBorder * scale);
        const int nativeWidth = std::round(width * scale);
        const int nativeRightBorder = std::round(rightBorder * scale);

        const int nativeLeft = std::round(x * scale);
        const int nativeRight = std::round((x + leftBorder + width + rightBorder) * scale);

        const int cmp = (nativeLeftBorder + nativeWidth + nativeRightBorder) - (nativeRight - nativeLeft);
        if (cmp == 0) {
            std::cout << x << " passed" << std::endl;
        } else if (cmp < 0) {
            std::cout << x << " failed (inflated)" << std::endl;
        } else {
            std::cout << x << " failed (deflated)" << std::endl;
        }
    }

    return 0;
}
