https://trac.webkit.org/wiki/LayoutUnit

https://en.wikipedia.org/wiki/Sexagesimal

https://en.wikipedia.org/wiki/Superior_highly_composite_number

  The first 15 superior highly composite numbers, 2, 6, 12, 60, 120, 360, 2520, 5040, 55440, 720720, 1441440, 4324320, 21621600, 367567200, 6983776800 (sequence A002201 in the OEIS) are also the first 15 colossally abundant numbers, which meet a similar condition based on the sum-of-divisors function rather than the number of divisors. Neither set, however, is a subset of the other.
