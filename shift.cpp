// shifting and snapping a segment to the pixel grid can change its size
#include <cmath>
#include <iostream>

int main()
{
    for (double dx = 0; dx < 10; dx += 0.05) {
        for (int x = 0; x < 50; ++x) {
            const int width = 5;
            
            const int x0 = std::round(x + dx);
            const int x1 = std::round(x + width + dx);

            if (x1 - x0 != width) {
                std::cout << "x: " << x << ", x0: " << x0 << ", x1: " << x1 << std::endl;
            }
        }
    }

    return 0;
}
